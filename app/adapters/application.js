import FirebaseAdapter from 'emberfire/adapters/firebase';

const { inject } = Ember;

export default FirebaseAdapter.extend({
  firebase: inject.service(),
  model() {
    return Ember.RSVP.hash({
      newsitems: this.store.findAll('newsitem'),
      categories: this.store.findAll('category')
    });
  }
});
