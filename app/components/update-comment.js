import Ember from 'ember';

export default Ember.Component.extend({
  updateCommentForm: false,
  actions: {
    updateCommentForm() {
      this.set('updateCommentForm', true);
    },
      updateComment(comment) {
        var params = {
          commenter_name: this.get('commenter_name'),
          commenter_comment: this.get('commenter_comment')
        }
          Object.keys(params).forEach(function(key) {
            if(params[key]!==undefined) {
              comment.set(key,params[key]);
            }
          });
          comment.save();
          this.set('updateCommentForm', false);
          this.transitionTo('newsitem');
        }
    },
    tagName: ''
});
