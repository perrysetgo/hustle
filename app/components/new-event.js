import Ember from 'ember';

export default Ember.Component.extend({
  showNewEventForm: false,
  actions: {
    showNewEventForm() {
      this.set('showNewEventForm', true);
    },

    saveEvent(category) {
      debugger; 
      var params = {
        name: this.get('name'),
        date: this.get('date'),
        time: this.get('time'),
        latitude: this.get('latitude'),
        longitude: this.get('longitude'),
        date_added: Date.now()
      };
      this.set('showNewEventForm', false);
      this.sendAction('saveEvent', params);
    }
  }
});
