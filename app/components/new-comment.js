import Ember from 'ember';

export default Ember.Component.extend({
  addComment: true,
  actions: {
    commentFormShow() {
      this.set('addComment', false);
    },
    saveComment() {
      var params = {
        commenter_name: this.get('commenter_name'),
        commenter_comment: this.get('commenter_comment'),
        newsitem: this.get('newsitem'),
        date_added: Date.now()
      };
      this.set('addComment', false);
      this.sendAction('saveComment', params);
    }
  }
});
