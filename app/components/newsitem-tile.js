import Ember from 'ember';

export default Ember.Component.extend({

  actions: {
    deleteNewsitem(newsitem) {
      if (confirm('Are you sure you want to delete this news item?')) {
        this.sendAction('destroyNewsitem', newsitem);
      }
    }
  }
});
