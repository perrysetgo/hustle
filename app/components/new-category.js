import Ember from 'ember';

export default Ember.Component.extend({
  addCategoryForm: false,
  actions: {
    categoryFormShow() {
      this.set('addCategoryForm', true);
    },

    saveCategory() {
      var params = {
        name: this.get('name'),
        description: this.get('description'),
        date_added: Date.now()
      };
      this.set('addCategoryForm', false);
      this.sendAction('saveCategory', params);
    }
  }
});
