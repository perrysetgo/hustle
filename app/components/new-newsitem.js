import Ember from 'ember';

export default Ember.Component.extend({
  addNewNewsitem: false,
  actions: {
    newsitemFormShow() {
      this.set('addNewNewsitem', true);
    },

    saveNewsitem(category) {
      var params = {
        headline: this.get('headline'),
        author: this.get('author'),
        category: category,
        image: this.get('image'),
        intro: this.get('intro'),
        content: this.get('content'),
        date_added: Date.now()
      };
      this.set('addNewNewsitem', false);
      this.sendAction('saveNewsitem', params);
    }
  }
});
