import Ember from 'ember';

export default Ember.Component.extend({

  actions: {
      updatecategory(category, params) {
          Object.keys(params).forEach(function(key) {
            if(params[key]!==undefined) {
              category.set(key,params[key]);
            }
          });
          category.save();
          this.transitionTo('index');
        },
        delete(category) {
          if (confirm('Are you sure you want to delete this news item?')) {
            this.sendAction('destroyCategory', category);
          }
        },
  },
  tagName: ''
});
