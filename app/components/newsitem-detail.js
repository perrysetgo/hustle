import Ember from 'ember';


export default Ember.Component.extend({

  actions: {
    saveComment(params) {
      this.set('addComment', false),
      this.sendAction('saveComment', params);
    },
  deleteComment(comment) {
    if (confirm('Are you sure you want to delete this comment?')) {
      this.sendAction('destroyComment', comment);
      }
    },
    update(newsitem,params) {
      this.sendAction('updateNewsitem', newsitem, params);
      }

  },
  tagName: ''
});
