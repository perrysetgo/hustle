import Ember from 'ember';

export default Ember.Component.extend({
  updateNewsitemForm: false,
  actions: {
    updateNewsitemForm() {
      this.set('updateNewsitemForm', true);
    },
    updateNewsitem(newsitem) {
      var params = {

        headline: this.get('headline'),
        author: this.get('author'),
        image: this.get('image'),
        intro: this.get('intro'),
        content: this.get('content'),
        date_added: Date.now()
      };
        Object.keys(params).forEach(function(key) {
          if(params[key]!==undefined) {
            newsitem.set(key,params[key]);
          }
        });
        newsitem.save();
        this.set('updateNewsitemForm', false);
      }
  },
  tagName: ''
});
