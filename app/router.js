import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('about_us');
  this.route('contact');
  this.route('newsitem', {path: '/newsitem/:newsitem_id'});
  this.route('category', {path: '/category/:category_id'});
  this.route('comment', {path: '/comment/:comment_id'});
  this.route('event');
});

export default Router;
