import DS from 'ember-data';

export default DS.Model.extend({
  name:DS.attr(),
  date:DS.attr(),
  time:DS.attr(),
  date_added: DS.attr(),
  latitude: DS.attr('number'),
  longitude: DS.attr('number')
});
