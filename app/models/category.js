import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr(),
  description: DS.attr(),
  newsitem: DS.hasMany('newsitem', { async: true }),
  date_added: DS.attr()
});
