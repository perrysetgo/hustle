import DS from 'ember-data';

export default DS.Model.extend({
  headline: DS.attr(),
  category: DS.belongsTo('category', { async: true }),
  comment: DS.hasMany('comment', { async: true }),
  author: DS.attr(),
  image: DS.attr(),
  intro:DS.attr(),
  content: DS.attr(),
  date_added: DS.attr()
});
