import DS from 'ember-data';

export default DS.Model.extend({
  newsitem: DS.belongsTo('newsitem', { async: true }),
  commenter_name: DS.attr(),
  commenter_comment: DS.attr(),
  date_added: DS.attr()
});
