import Ember from 'ember';
import Resolver from './resolver';
import loadInitializers from 'ember-load-initializers';
import config from './config/environment';

let App;

Ember.MODEL_FACTORY_INJECTIONS = true;

App = Ember.Application.extend({
  modulePrefix: config.modulePrefix,
  podModulePrefix: config.podModulePrefix,
  Resolver
});

loadInitializers(App, config.modulePrefix);

Ember.Route.reopen({
  model() {
    return Ember.RSVP.hash({
      categories: this.store.findAll('category'),
      comments: this.store.findAll('comment'),
      newsitems: this.store.findAll('newsitem')
    });
  }
});

export default App; 
