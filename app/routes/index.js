import Ember from 'ember';


export default Ember.Route.extend({
  model() {
    return Ember.RSVP.hash({
      newsitems: this.store.findAll('newsitem'),
      categories: this.store.findAll('category'),
      comments:this.store.findAll('comment')
    });
  },

  actions: {
    saveNewsitem(params) {
      var newNewsitem = this.store.createRecord('newsitem', params);
      newNewsitem.save();
      this.transitionTo('index');
    },
    saveCategory(params) {
      var newCategory = this.store.createRecord('category', params);
      newCategory.save();
      this.transitionTo('index');
    },
    // destroyNewsitem(newsitem, params) {
    //   newsitem.destroyRecord().then(function() {
    //     var getCategory = params.category;
    //     getCategory.save();
    //   });
    //   this.transitionTo('index');
    // },
    destroyCategory(category) {
      category.destroyRecord();
      this.transitionTo('index');
    },
      updateComment(comment, params) {
          Object.keys(params).forEach(function(key) {
            if(params[key]!==undefined) {
              comment.set(key,params[key]);
            }
          });
          comment.save();
          this.transitionTo('newsitem');
        }
  }
});
