import Ember from 'ember';

export default Ember.Route.extend({
  model(params) {
    return this.store.findRecord('category', params.category_id);
    return this.store.findAll('newsitem'); //this may not be what i need
  },

  actions: {
    destroyCategory(category) {
      category.destroyRecord();
      this.transitionTo('index');
    },
    saveNewsitem(params) {
      var newNewsitem = this.store.createRecord('newsitem', params);
      var saveCategory = params.category;
      saveCategory.get('newsitem').addObject(newNewsitem);
      newNewsitem.save().then (function () {
        return saveCategory.save();
        });
      this.transitionTo('category', params.category);
    }
  }
});
