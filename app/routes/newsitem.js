import Ember from 'ember';


export default Ember.Route.extend({
  model(params) {
    return this.store.findRecord('newsitem', params.newsitem_id);
    return this.store.findAll('comment', params.newsitem_id);
  },

  actions: {

    saveComment(params) {
      var newComment = this.store.createRecord('comment', params);
      var newsitem = params.newsitem;
      newsitem.get('comment').addObject(newComment);
      newComment.save().then(function() {
        return newsitem.save();
      });
      this.transitionTo('newsitem', params.newsitem);
    },

    destroyComment(comment) {
        comment.destroyRecord().then(function() {
          var newsitem = params.newsitem;
          newsitem.save();
        });
        this.transitionTo('index');
      },
      updateComment(comment, params) {
          Object.keys(params).forEach(function(key) {
            if(params[key]!==undefined) {
              comment.set(key,params[key]);
            }
          });
          comment.save();
          this.transitionTo('index');
        }
  }
});
